Course Booking Application Data Models
-- both embedded and normalized 
===== users ===== 
{
  _id: ObjectId <- primary key; userId
  firstName: string,
  lastName: string,
  age:string, 
  gender:string,
  email:string,
  password:string,
  mobileNo:string,
  isAdmin: boolean,
  enrollments: [{
    courseId: courseId, <- foreign key
    status:string,
    enrolledOn:date,
    paymentStatus: transactionId
  }]
}

==== courses ====
{
  _id: ObjectId <- mongoDB sets this automatically; primary key; courseId
  name: string,
  description: string, 
  price: number, 
  isActive: boolean, 
  createdOn: date, 
  enrollees: [{
    userId: string, <- foreign key
    enrolledOn: date
  }]
}
==== transactions ====
{
  _id: ObjectId, <- transactionId; primary
  userId: string, <- foreign
  courseId: string, <- foreign
  isPaid: boolean, 
  totalAmount: number,
  paymentMethod: string,
  dateTimeCreated: date
}

identifying relationships between data models 
  - primary key and foreign key 
  - relationships can have 3 types 
    1. one to one - a person can only have one employee ID
    2. one to many - a person can have one or more email addresses; a person can be enrolled in one/more courses 
    3. many to many - a book written by multuple authors, an author can write many books 

Data Model Design 
- the key consideration for the structure of your documents 
- many types
- can use multiple data model designs bc mongoDB is a flexible data structure 

==== EMBEDDED DATA MODELS ====
- stores another document in another document 
- will set subdocument info - hardcode the information 
- pros: easier to set bc manually setting the properties, easy to input data 
- cons: heavy on the document, takes up more storage, difficult if there are a lot of properties, higher chance of duplication of data 
- good for small applications 
{
  name: "ABC company",
  contact: '0912389754",
  branches: <-- subdocument/embedded 
  { 
    name: "branch1",
    address: "QC",
    contact: "0912384632"
  }
}

{
  userName: "user123",
  contact: {
    mobileNo: "09178346",
    phoneNo: "43905438",
    email: "user@email.com",
    faxNo: "329048230918092184"
  },
  
}

==== NORMALIZED/REFERENCED DATA MODELS ====
- connect data models with an id 
- no need to embed the branch model with the supplier info because the supplier can be accessed with a different model and can find the supplier info with the supplier id in the branch model and vice versa 
- pros: lightweight data wise, separation of concern, quick data retrieval, quick reference, good for large applications 
- cons: complicated set up 
//branch model 
{
  name: string,
  address: string,
  city: string, 
  supplier_id: <supplier_id>
}

//suppliers model
{
  name: string, 
  contact: 
  branch: [
    <branch_id>
  ]
}